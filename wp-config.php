<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mps');

/** MySQL database username */
define('DB_USER', 'mps');

/** MySQL database password */
define('DB_PASSWORD', '74rE7NKUR5');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&O8m^l/Tb-9q&+xRr$qD)NKW+g/VY}EMyq`1u&H~6iVngGzA<OWkjZ-Z n6[}lr.');
define('SECURE_AUTH_KEY',  '3bL->G@gtrtviVO^xWT.B.b!NZ>uQ$-h[ l^]+a<5g86KOA>ai?v1CD9JShL,Q*U');
define('LOGGED_IN_KEY',    'xDI5!{q[7p,)I1}rRPk40]<~#%BPXp lb;syTcJ$ZfqpER4[]n2+zMM)3K{uU]Ch');
define('NONCE_KEY',        'bvA?5n8(VA%6*Zisd_zUPYI.-vE--cf/{ hR*Q#zJ.(B ep)!M9|4 uK#9g6B$z*');
define('AUTH_SALT',        'QA`jGb|~Ppq5gwJ6Dl88R09{ hU,XhNo.m/rD;*-S6L1^@s2T.u|3_^kty]kTeQ!');
define('SECURE_AUTH_SALT', ';EZa4e]|D2oW$fD~wcqxB6MujH|!NzeE7;Bk8tHN}c.`u`U-XdS@:#9:<++/g|^O');
define('LOGGED_IN_SALT',   'DL,A+E`87MGBO 61EcZgIqPNSJM#v]VJ]VLk}| kV][P2yV.6AVr=CF7),|-u_A[');
define('NONCE_SALT',       'r?zw6*_U<MLUHm 0RbbpSW1<R$t?4y-$@R3V)fi5|/-wc<o||p@H<m+PYd,oFfu4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
